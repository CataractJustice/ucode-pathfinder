#include "pathfinder.h"
void printpath(island_t* is) {
    mx_printstr("========================================\n");

    island_t* reverse[64];
    int reverse_c = 0;
    island_t* end = is;
    island_t* start;

    while(is) {
        reverse[reverse_c] = is;
        is = is->path_back;
        reverse_c++;
    }

    start = reverse[reverse_c - 1];

    mx_printstr("Path: ");
    mx_printstr(start->name);
    mx_printstr(" -> ");
    mx_printstr(end->name);
    mx_printstr("\nRoute: ");
    int i = reverse_c;
    while(i) {
        i--;
        mx_printstr(reverse[i]->name);
        if(i) {
            mx_printstr(" -> ");
        }
    }
    mx_printstr("\nDistance: ");

    i = reverse_c - 1;
    while(i) {
        i--;
        mx_printint(reverse[i]->back_bridge);
        if(i) {
            mx_printstr(" + ");
        }
    }
    if(reverse_c > 2) {
        mx_printstr(" = ");
        mx_printint(end->mdst);
    }
    mx_printstr("\n========================================\n");
}
