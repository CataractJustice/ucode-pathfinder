#define BRIDGES_STACK_CAPACITY 64
#define ISLANDS_STACK_CAPACITY 64
#include "pathfinder.h"

island_t** island_stack;
int island_stack_size = 0;
int bridgesum = 0;

void islands_init() {
    island_stack = malloc(sizeof(island_t*) * ISLANDS_STACK_CAPACITY);
}

void clear_path_info() {
    for(int i = 0; i < island_stack_size; i++) {
        island_stack[i]->mdst = 1000000;
        island_stack[i]->path_back = NULL;
    }
}

island_t* new_island(char* island_name) {
    island_t* island = malloc(sizeof(island_t));
    island->islands = malloc(sizeof(island_t*) * BRIDGES_STACK_CAPACITY);
    island->bridges = malloc(sizeof(int) * BRIDGES_STACK_CAPACITY);
    island->name = island_name;

    island_stack[island_stack_size] = island;
    island_stack_size++;
    return island_stack[island_stack_size - 1];
}

island_t* get_island_by_id(int id) {
    return island_stack[id];
}

island_t* get_island_by_name(char* name) {
    for(int i = 0; i < island_stack_size; i++) {
        if(mx_strcmp(island_stack[i]->name, name) == 0) {
            return island_stack[i];
        }
    }
    return NULL;
}

void new_bridge(island_t* from, island_t* to, int length) {

    if(bridgesum > bridgesum + length) {
        mx_printerr("error: sum of bridges lengths is too big\n");
        exit(-1);
    }
    bridgesum += length;

    for(int i = 0; i < from->bridge_counter; i++) {
        if(from->islands[i] == to) {
            mx_printerr("error: duplicate bridges\n");
            exit(-1);
        }
    }

    from->bridges[from->bridge_counter] = length;
    to->bridges[to->bridge_counter] = length;
    from->islands[from->bridge_counter] = to;
    to->islands[to->bridge_counter] = from;
    from->bridge_counter++;
    to->bridge_counter++;
}

void islands_free() {
    for(int i = 0; i < island_stack_size; i++) {
        free(island_stack[i]->name);
        free(island_stack[i]->islands);
        free(island_stack[i]->bridges);
        free(island_stack[i]);
    }
    free(island_stack);
}

int get_islands_count() {
    return island_stack_size;
}
