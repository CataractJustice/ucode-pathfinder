#include "pathfinder.h"

typedef struct path_s {
    island_t* island;
    int length;
}   path_t;

#define path_stack_capacity 256
int path_stack_size = 0;
int path_stack_buffer_size = 0;
path_t path_stack[path_stack_capacity];
path_t path_stack_buffer[path_stack_capacity];

void pathfind_step() {
    path_stack_buffer_size = path_stack_size;
    path_stack_size = 0;
    for(int i = 0; i < path_stack_buffer_size; i++) {
        path_stack_buffer[i] = path_stack[i];
    }

    for(int i = 0; i < path_stack_buffer_size; i++) {

        path_t* pathr = &path_stack_buffer[i];

        for(int j = 0; j < pathr->island->bridge_counter; j++) {
            path_t newpath_node = { pathr->island->islands[j], pathr->island->bridges[j] + pathr->length };
            if(newpath_node.length < pathr->island->islands[j]->mdst) {
                path_stack[path_stack_size] = newpath_node;
                path_stack[path_stack_size].island->mdst = newpath_node.length;
                path_stack[path_stack_size].island->path_back = pathr->island;
                path_stack[path_stack_size].island->back_bridge = pathr->island->bridges[j];
                path_stack_size++;
            }
        }
    }


}

void pathfind_single(island_t* is) {
    clear_path_info();
    path_stack[0].island = is;
    path_stack[0].length = 0;
    is->mdst = 0;
    path_stack_size = 1;
    while(path_stack_size) {
        pathfind_step();
    }
}
