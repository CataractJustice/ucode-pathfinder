
#include "pathfinder.h"
void parse_islands(char* file) {
    char* s = mx_file_to_str(file);

    if(!s) {
        mx_printerr("error: file [filename] does not exist\n");
        exit(-1);
    }

    if(!*s) {
        mx_printerr("error: file [filename] is empty\n");
        exit(-1);
    }

    int l = 1;

    char* c = s;
    while(*c != '\n') {
        if(!mx_isdigit(*c)) {
            mx_printerr("error: line 1 is not valid\n");
            exit(-1);
        }
        c++;
    }
    *c = 0;
    int islands_count = mx_atoi(s);

    c++;
    l++;
    while(*c != 0) {
        char* namestart = c;
        char* namea;
        char* nameb;
        char* lenstr;
        bool e = false;

        while(*c != '-') {
            if(!mx_isalpha(*c)) {
                e = true;
            }
            c++;
        }
        namea = malloc(c - namestart);
        mx_strncpy(namea, namestart, c - namestart);
        namea[(c - namestart)] = 0;
        c++;

        namestart = c;
        while(*c != ',') {
            if(!mx_isalpha(*c)) {
                e = true;
            }
            c++;
        }
        nameb = malloc(c - namestart);
        mx_strncpy(nameb, namestart, c - namestart);
        nameb[(c - namestart)] = 0;
        c++;

        namestart = c;
        while(*c != '\n' && *c != 0) {
            if(!mx_isdigit(*c)) {
               e = true;
            }
            c++;
        }
        lenstr = malloc(c - namestart);
        mx_strncpy(lenstr, namestart, c - namestart);
        lenstr[(c - namestart)] = 0;
        c++;
        
        island_t* ia = get_island_by_name(namea);
        island_t* ib = get_island_by_name(nameb);
        if(!ia)
            ia = new_island(namea);
        if(!ib)
            ib = new_island(nameb);
        new_bridge(ia, ib, mx_atoi(lenstr));
        free(lenstr);
        //mx_printstr(namea);
        //mx_printchar('\n');
        //mx_printstr(nameb);
        //mx_printchar('\n');
        //mx_printstr(lenstr);
        //mx_printchar('\n');

        if(e) {
            mx_printerr("error: line ");
            char* lstr = mx_itoa(l);
            mx_printerr(lstr);
            free(lstr);
            mx_printerr(" is not valid\n");
            exit(-1);
        }

        l++;
    }

    if(get_islands_count() != islands_count) {
        mx_printerr("error: invalid number of islands\n");
        exit(-1);
    }
}
