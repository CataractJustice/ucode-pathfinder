#include "pathfinder.h"
int main(int argc, char** argv) {

    if(argc != 2) {
        mx_printerr("usage: ./pathfinder [filename]");
        exit(-1);
    }


    islands_init();
    parse_islands(argv[1]);
    for(int i = 0; i < get_islands_count(); i++) {
        pathfind_single(get_island_by_id(i));
        for(int j = i + 1; j < get_islands_count(); j++) {
            printpath(get_island_by_id(j));
        }
    }
    islands_free();
}
