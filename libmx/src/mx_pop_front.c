#include "libmx.h"

void mx_pop_front(t_list **list) {
	if(!list) return;
	if(!*list) return;

	t_list* buff = (*list)->next;
	free(*list);
	*list = buff;
}


