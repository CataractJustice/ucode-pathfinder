#include "libmx.h"

static int file_len(const char *file) {
    int fl = open(file, O_RDONLY);
    int c = 0;
    int len = 0;
    char ch;
    c = read(fl, &ch, 1);
    while (c > 0) {
        c = read(fl, &ch, 1);
        len++;
    }
    close(fl);
    return len;
}

char *mx_file_to_str(const char *file) {
    int fl = open(file, O_RDONLY);
    int c = 0;
    int size = 0;
    if (fl == -1) {
        close(fl);
        return NULL;
    }
    size = file_len(file);
    if (size == 0) {
        return mx_strnew(0);
    }
    char *newstr = mx_strnew(size);
    c = read(fl, newstr, size);
    close(fl);
    return newstr;
}

