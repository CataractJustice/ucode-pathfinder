#include "libmx.h"

void mx_pop_back(t_list **list) {
	if(!list) return;
	if(!*list) return;

	t_list* end = *list;
	t_list* previous = NULL;
	
	while(end->next) {
		previous = end;
		end = end->next;
	}
	if(end == *list) {
		*list = NULL;
	}
	free(end);
	
	if(previous) {
		previous->next = NULL;
	}
}


