
void mx_printchar(char c);

void mx_printint(int num) {

	if(num == 0) {
		mx_printchar('0');
		return;
	}

	int length = 0;
	char buff[32];
	int n = num;

	while (n)
	{
		int d = (n % 10);
		buff[length] = '0' + (d > 0 ? d : -d);
		n /= 10;
		length++;
	}
	
	if(num < 0) {
		buff[length] = '-';
		length++;
	}
	
	for(int i = 1; i <= length; i++)
		mx_printchar(buff[length - i]);
}

