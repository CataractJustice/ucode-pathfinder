int mx_sqrt(int x) {
	int r = 1;
	
	if(x <= 0)
		return 0;
	
	int min = 1;
	int max = x / 3 + 1;
	int s;
	
	if(max > 46340) max = 46340;
	
	while(max - min > 1) {
		s = r * r;
		
		if(s == x)
			return r;
		else {
			if(s > x) {
				max = r;
			}
			else {
				min = r;
			}
			r = (min + max) / 2;
		}
	}
	
	for(int i = min; i <= max; i++)
		if(i * i == x) return i;
	
	return 0;
}

