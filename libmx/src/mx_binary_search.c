#include "libmx.h"

int mx_binary_search(char **arr, int size, const char *s, int *count) {
    int min = 0;
    int max = size-1;

	*count = 0;

	while (min <= max) {
	
	(*count)++;
	
	int middle = (min + max) / 2;

	if (mx_strcmp(arr[middle], s) == 0) {
		return middle;
	}
	
	else if (mx_strcmp(arr[middle], s) >= 0) {
		max = middle - 1;
	}
	else if (mx_strcmp(arr[middle], s) <= 0) {
		min = middle + 1;
		}
	}
	*count = 0;
	return -1;
}

