BIN=pathfinder
CLFLAGS=-std=c11 -Wall -Wextra -Werror -Wpedantic
all: $(BIN)
$(BIN):
	make all -sC libmx
	mkdir -p obj
	clang $(CLFLAGS) -c src/*.c -I inc -I libmx/inc
	mv *.o obj
	clang $(CLFLAGS) ./obj/*.o -L libmx -lmx -o $(BIN)

clean:
	rm -rf obj;
	make clean -sC libmx

uninstall: clean
	rm -f libmx/libmx.a
	rm -f $(BIN)

reinstall: uninstall all
