#include "libmx.h"

void parse_islands(char* file);


typedef struct island_s {
    char* name;
    struct island_s** islands;
    int* bridges;
    int bridge_counter;
    int mdst;
    int back_bridge;
    struct island_s* path_back;
} island_t;

void islands_init();
void clear_path_info();
island_t* new_island(char* island_name);
island_t* get_island_by_id(int id);
island_t* get_island_by_name(char* name);
void new_bridge(island_t* from, island_t* to, int length);
void islands_free();
void pathfind_single(island_t* is);
void printpath(island_t* is);
int get_islands_count();
